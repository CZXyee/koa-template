# Koa 模板

#### 介绍

一个 Koa + Typescript 起手式，配置了一些基本的包：

-   `koa` - 框架
-   `koa-router` -  body 解析器
-   `koa-body` - 请求解析
-   `koa-static` - 静态文件服务
-   `koa-mount` - 挂载中间件
-   `jsonwebtoken` - jwt
-   `log4js` - 日志
-   `axios` - 请求
-   `joi` - 数据校验
-   `dotnet` - 环境变量

ps: 没有用OOP，函数式。

#### 使用

1. 拉取项目
2. 安装依赖 pnpm install
3. 启动项目 pnpm dev
